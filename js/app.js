document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById('make-group').addEventListener('submit', function (e) {
        let newGroup = new Generator(document.getElementById('nb-group').value, 'result', document);
        newGroup.init();

        //Stop propagation
        e.preventDefault();
    })
});

var students = [
    "Teodor",
    "Naima",
    "Nathan",
    "Jimmy",
    "Cherif",
    "Nicolas",
    "Loup",
    "Thomas",
    "William",
    "Alexis",
    "Dylan",
    "Mohamed"
];

/**
 * Generate random groups
 */
class Generator {
    constructor(numberGroups, result, d) {
        this.numberGroups = numberGroups;
        this.result = d.getElementById(result);
        this.groups = [];
        this.restStudents = students.slice();
        this.document = d;
    }

    create() {
        let i = 0;

        // generate group
        while (this.restStudents.length)
        {
            let index = Math.floor(Math.random() * Math.floor(this.restStudents.length)),
                indexGroup = i % parseFloat(this.numberGroups) + 1;

            // Create array of group if not exist
            if (!this.groups[indexGroup])
            {
                this.groups[indexGroup] = [];
            }

            // add studient in target group
            this.groups[indexGroup].push(this.restStudents[index]);

            // delete studient of rest
            this.restStudents.splice(index, 1);

            i++;
        }
    }

    show(group, index) {
        let newGroup = this.document.createElement('div');
        let title = this.document.createElement('h2');
        let list = this.document.createElement('ol');

        title.appendChild(this.document.createTextNode('Groupe ' + index));

        group.forEach((element) => {
            let newItem = this.document.createElement('li');
            let content = this.document.createTextNode(element);

            newItem.appendChild(content);

            list.appendChild(newItem);
        });

        newGroup.appendChild(title);
        newGroup.appendChild(list);

        this.result.appendChild(newGroup);
    }

    init() {
        this.create();

        this.result.innerHTML = '';
        this.groups.forEach((element, index) => {
            this.show(element, index)
        })
    }
}
